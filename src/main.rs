use std::path::PathBuf;
use clap::{command, arg, value_parser};
use gtk::glib;

use rustikal::Rustikal;

fn main() -> glib::ExitCode {
    let rustikal = Rustikal::new();

    let matches = command!()
        .args([
            arg!(-c --configdir <DIRECTORY> "Choose the directory for the session file")
                .required(false)
                .value_parser(value_parser!(PathBuf)),
            arg!(-d --dir <DIRECTORY> "Choose the start directory")
                .required(false)
                .value_parser(value_parser!(PathBuf)),
            arg!(-e --erase "Don't save session when exiting")
                .required(false),
            arg!(-n --new "Start new session. Don't load session from session file")
                .required(false),
            arg!(-s --session <FILENAME> "Name of the session file")
                .required(false)
        ])
        .try_get_matches();

    match matches {
        Err(e) => match e.kind() {
            clap::error::ErrorKind::DisplayHelp |
            clap::error::ErrorKind::DisplayVersion => {
                println!("{}", e);
                glib::ExitCode::SUCCESS
            },
            _ => {
                eprintln!("An error occured wile parsing arguments: {}", e.to_string());
                glib::ExitCode::FAILURE
            }
        }

        Ok(matches) => {
            if let Some(configdir) = matches.get_one::<PathBuf>("configdir") {
                rustikal.set_config_dir(configdir.to_path_buf());
            }
            if let Some(dir) = matches.get_one::<PathBuf>("dir") {
                rustikal.set_start_dir(dir.to_path_buf());
            }
            if matches.get_flag("erase") {
                rustikal.set_erase(true);
            }
            if matches.get_flag("new") {
                rustikal.set_new(true);
            }
            if let Some(session) = matches.get_one::<String>("session") {
                rustikal.set_session_file(session.to_string());
            }
            rustikal.run()
        }
    }
}