use std::rc::Rc;

use gtk::prelude::*;
use gtk::{AlertDialog, Window};

pub async fn alert<W: IsA<Window>>(window: Rc<W>, msg: String) {
    let dialog = AlertDialog::builder().modal(true).message(msg).build();

    dialog.show(Some(&*window));
}
