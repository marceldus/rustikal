use std::path::PathBuf;
use serde::{Deserialize, Serialize};

#[derive(Default, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct Source {
    path: PathBuf,
}

impl Source {
    pub fn new() -> Source {
        let path = dirs::home_dir().unwrap();
        Source { path }
    }

    pub fn new_from_path(path: PathBuf) -> Source {
        Source { path }
    }

    pub fn get_path(&self) -> &PathBuf {
        &self.path
    }

    #[allow(dead_code)]
    pub fn set_path(&mut self, path: PathBuf) {
        self.path = path;
    }
}