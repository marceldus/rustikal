use std::rc::Rc;
use std::cell::RefCell;

use glib::{clone, MainContext};
use gtk::prelude::*;
use gtk::{
    gio, glib, ApplicationWindow, FileDialog, ScrolledWindow, Stack
};

use crate::session::Session;
use crate::source::Source;
use crate::dir::dir_view;

pub fn setup_actions(window: &ApplicationWindow, stack: &Stack, session: Rc<RefCell<Session>>) {
    let main_ctxt = MainContext::default();
    let action_open = gio::SimpleAction::new("open", None);
    action_open.connect_activate(clone!(@weak window, @weak stack => move |_, _| {
        let sc = session.clone();
        main_ctxt.spawn_local(clone!(@weak window, @weak stack => async move {
            let file_dialog = FileDialog::builder().modal(false).build();
            let result = file_dialog.select_folder_future(Some(&window)).await;
            match result {
                Ok(file) => {
                    if let Some (path) = file.path() {
                        let sw = ScrolledWindow::builder()
                            .child(&dir_view::get_dir_view(&path).unwrap())
                            .build();
                        stack.add_titled(&sw, path.to_str(), path.to_str().unwrap());
                        let source = Source::new_from_path(path);
                        sc.borrow_mut().add_source(source);
                    }
                }
                Err(e) => eprintln!("Error: {e:#?}"),
            };
        }));
    }));
    window.add_action(&action_open);
}