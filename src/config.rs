use std::path::PathBuf;

use crate::APP_ID;

pub struct Config {
    config_path: Option<PathBuf>,
    session_file: String,
    start_dir: Option<PathBuf>,
    erase: bool,
    new: bool,
}

impl Config {
    pub fn new() -> Config {
        let mut config_path: Option<PathBuf> = None;
        if let Some(mut tmp_config_path) = dirs::config_local_dir() {
                tmp_config_path.push(APP_ID);
                config_path = Some(tmp_config_path);
         }

        Config {
            session_file: String::from("session.json"),
            config_path,
            start_dir: None,
            erase: false,
            new: false,
        }
    }

    pub fn set_session_file(&mut self, session_file: String) {
        self.session_file = session_file;
    }

    pub fn get_session_file(&self) -> &String {
        &self.session_file
    }

    pub fn set_config_path(&mut self, config_path: Option<PathBuf>) {
        self.config_path = config_path;
    }

    pub fn get_config_path(&self) -> &Option<PathBuf> {
        &self.config_path
    }

    pub fn set_start_dir(&mut self, start_dir: PathBuf) {
        self.start_dir = Some(start_dir);
    }

    pub fn get_start_dir(&self) -> &Option<PathBuf> {
        &self.start_dir
    }

    pub fn set_erase(&mut self, erase: bool) {
        self.erase = erase;
    }

    pub fn get_erase(&self) -> bool {
        self.erase
    }

    pub fn set_new(&mut self, new: bool) {
        self.new = new;
    }

    pub fn get_new(&self) -> bool {
        self.new
    }
}