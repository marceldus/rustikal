//! Maybe one day this will be a file manager with an integration for the German "besonderes elektronisches Anwaltspostfach" (special electronic mailbox for lawyers) and messages sent via it.

mod config;
mod session;
mod source;
mod actions;
mod dir;
mod alert;

use std::rc::Rc;
use std::cell::RefCell;
use std::path::PathBuf;

use glib::{clone, MainContext};
use gtk::prelude::*;
use gtk::{gio, glib, Application, ApplicationWindow, Builder, ScrolledWindow, Stack, Window};

use crate::config::Config;
use crate::source::Source;
use crate::actions::setup_actions;
use crate::dir::dir_view;
use crate::alert::alert;
use crate::session::Session;

const APP_ID: &str = "org.keienb.rustikal";

pub struct Rustikal {
    app: Application,
    config: Rc<RefCell<Config>>,
    session: Rc<RefCell<Session>>,
}

impl Rustikal {
    pub fn new() -> Rustikal {
        let app = Application::builder().application_id(APP_ID).build();
        let config = Rc::new(RefCell::new(Config::new()));
        let cc = config.clone();
        let session: Rc<RefCell<Session>> = Rc::new(RefCell::new(Session::new()));
        let sc = session.clone();

        app.connect_startup(clone!(@weak app => move |_| {
            app.set_accels_for_action("win.open", &["<Ctrl>o"]);
            app.set_accels_for_action("window.close", &["<Ctrl>q"]);
        }));

        app.connect_activate(clone!(@weak app => move |_| {
            gio::resources_register_include!("org_keienb_rustikal.gresource")
                .expect("Failed to register resources.");
            let builder = Builder::from_resource("/org/keienb/rustikal/window.ui");
            let window :ApplicationWindow = builder.object("app_window")
                .expect("Failed to load application window from resource");
            let stack: Stack = builder.object("stack")
                .expect("Failed to load stack from resource.");

            let mut ms = sc.borrow_mut();
            if let Some(start_dir) = cc.borrow().get_start_dir() {
                ms.add_source(Source::new_from_path(start_dir.to_path_buf()));
            }
            if ms.get_sources().len() == 0 {
                ms.add_source(Source::new());
            }

            ms.get_mut_sources().retain(|source| {
                let path = source.get_path();
                let dv = dir_view::get_dir_view(path);
                match dv {
                    Ok(dv) => {
                        let sw = ScrolledWindow::builder()
                            .child(&dv)
                            .build();
                        stack.add_titled(&sw, path.to_str(), path.to_str().unwrap());
                        if let Some(start_dir) = cc.borrow().get_start_dir() {
                            if let Some(start_dir) = start_dir.to_str() {
                                if start_dir == path.to_str().unwrap() {
                                    stack.set_visible_child_name(start_dir);
                                }
                            }
                        }
                        true
                    },
                    Err(e) => {
                        MainContext::default()
                            .spawn_local(
                                alert(window.clone().into(), 
                                format!(
                                    "Error while adding directory {}: {}",
                                    path.to_str().unwrap_or_else(|| {"(no valid pathname found)"}),
                                    e.to_string()
                                )
                            )
                        );
                        false
                    },
                }
            });

            setup_actions(&window, &stack, sc.clone());
            app.add_window(&window);
            window.present();
        }));

        Rustikal { app, config, session }
    }

    pub fn run(self) -> glib::ExitCode {
        let cc = self.config.clone();
        let sc = self.session.clone();
        if !cc.borrow().get_new() {
            if let Some(ref config_path) = cc.borrow().get_config_path() {
                if let Ok(tmp_session) = Session::new_from_file(config_path, &cc.borrow().get_session_file()) {
                    let mut ms = sc.borrow_mut();
                    *ms = tmp_session;
                }
            }
        }
        let empty: Vec<String> = vec![];
        let result = self.app.run_with_args(&empty);
        if !cc.borrow().get_erase() {
            if let Some(config_path) = cc.borrow().get_config_path() {
                let ms = sc.borrow();
                ms.save(config_path.to_path_buf(), cc.borrow().get_session_file());
            }
        }
        result
    }

    pub fn get_window(&self) -> Option<Window> {
        if let Some(window) = self.app.active_window() {
            return Some(window);
        }
        None
    }

    pub fn get_stack(&self) -> Option<Stack> {
        if let Some(window) = self.get_window() {
            if let Ok(stack) = window.child().unwrap().downcast::<Stack>() {
                return Some(stack);
            }
        };
        None
    }

    pub fn set_config_dir(&self, path: PathBuf) {
        self.config.clone().borrow_mut().set_config_path(Some(path));
    }

    pub fn set_start_dir(&self, path: PathBuf) {
        self.config.clone().borrow_mut().set_start_dir(path);
    }

    pub fn set_erase(&self, erase: bool) {
        self.config.clone().borrow_mut().set_erase(erase);
    }

    pub fn set_new(&self, new: bool) {
        self.config.clone().borrow_mut().set_new(new);
    }

    pub fn set_session_file(&self, session_file: String) {
        self.config.clone().borrow_mut().set_session_file(session_file);
    }
}
