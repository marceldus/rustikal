use std::fs;
use std::io::Error;
use std::path::PathBuf;

use gtk::prelude::*;
use gtk::{
    ColumnView, ColumnViewColumn, Label, ListItem, MultiSelection, SignalListItemFactory, StringList,
    StringObject, Widget,
};

/// Returns a gtk4::ColumnView, wrapped in a Result, containing a directory in the file system as model.
pub fn get_dir_view(path: &PathBuf) -> Result<ColumnView, Error> {
    let files = collect_files(path);
    match files {
        Ok(files) => {
            let model: StringList = files.into_iter().collect();

            let factory = SignalListItemFactory::new();
            factory.connect_setup(move |_, list_item| {
                let label = Label::builder().xalign(0.0).build();
                let list_item = list_item
                    .downcast_ref::<ListItem>()
                    .expect("Needs to be ListItem");
                list_item.set_child(Some(&label));

                list_item
                    .property_expression("item")
                    .chain_property::<StringObject>("string")
                    .bind(&label, "label", Widget::NONE);
            });

            let selection_model = MultiSelection::new(Some(model));

            let colv = ColumnView::builder()
                .model(&selection_model)
                .show_row_separators(true)
                .build();
            let col = ColumnViewColumn::builder()
                .title("Filename")
                .expand(true)
                .resizable(true)
                .factory(&factory)
                .build();
            colv.append_column(&col);

            return Ok(colv);
        },
        Err(e) => Err(e),
    }
}

/// Returns a [Vec]<[String]> containing the files in the chosen directory, wrapped in a result
fn collect_files(path: &PathBuf) -> Result<Vec<String>, Error> {
    let mut v: Vec<String> = Vec::new();
    let p = fs::read_dir(path.as_path());
    match p {
        Err(e) => {
            eprintln!("Error reading directory: {e}");
            return Err(e);
        },
        Ok(entries) => {
            for e in entries {
                if let Ok(e) = e {
                    if let Ok(filename) = e.file_name().into_string() {
                        v.push(filename);
                    }
                }
            }
        }
    }
    Ok(v)
}
