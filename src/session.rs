use std::{io::Error, fs, path::PathBuf, collections::HashSet};

use serde::{Deserialize, Serialize};

use crate::source::Source;

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct Session {
    sources: HashSet<Source>,
}

impl Session {
    pub fn new() -> Session {
        Session {
            sources: HashSet::new(),
        }
    }

    pub fn new_from_file(config_path: &PathBuf, session_file: &String) -> Result<Session, Error> {
        let session: Session;
        let mut path: PathBuf = PathBuf::new();
        path.push(config_path);
        path.push(session_file);
        let result = fs::File::open(path);
        match result {
            Ok(file) => {
                let result = serde_json::from_reader(file);
                match result {
                    Ok(jsession) => {
                        session = jsession;
                        Ok(session)
                    }
                    Err(e) => Err(e.into()),
                }
            }
            Err(e) => Err(e),
        }
    }

    pub fn get_sources(&self) -> &HashSet<Source> {
        &self.sources
    }

    pub fn get_mut_sources(&mut self) -> &mut HashSet<Source> {
        &mut self.sources
    }

    pub fn add_source(&mut self, source: Source) -> bool {
        self.sources.insert(source)
    }
    
    pub fn save(&self, config_path: PathBuf, session_file: &String) {
        let mut path= config_path;
        let result = fs::create_dir_all(&path);
        match result {
            Ok(_) => {
                path.push(session_file);
                let result = fs::File::create(path);
                match result {
                    Ok(file) => {
                        let result = serde_json::to_writer_pretty(file, &self);
                        match result {
                            Ok(_) => (),
                            Err(e) => eprintln!("Error while wrting to session file: {}", e.to_string()),
                        }
                    }
                    Err(e) => eprintln!("Error while creating session file: {}", e.to_string()),
                }
            }
            Err(e) => eprintln!("Error while creating directory for session file: {}", e.to_string()),
        }
    }

}